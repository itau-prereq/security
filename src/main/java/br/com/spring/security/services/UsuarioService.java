package br.com.spring.security.services;

import br.com.spring.security.auth.AuthUsuario;
import br.com.spring.security.models.Usuario;
import br.com.spring.security.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Usuario salvarUsuario(Usuario usuario){
        String senha = usuario.getSenha();

        usuario.setSenha(encoder.encode(senha));

        return usuarioRepository.save(usuario);
    }

    public Iterable<Usuario> retornaTodosUsuarios(){
        return usuarioRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario == null){
            throw new UsernameNotFoundException("Usuário não encontrado!");
        }
        AuthUsuario authUsuario = new AuthUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return authUsuario;
    }
}
