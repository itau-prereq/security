package br.com.spring.security.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "T5Y8RGUIFO0CELDSBX;VH,";

    private static final Long EXPIRATION_TIME = 860000l;

    public String gerarToken(String email){
        Date dataExpiracao = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
        String token = Jwts.builder().setSubject(email)
                                     .setExpiration(dataExpiracao) //Set validade
                                     .signWith(SignatureAlgorithm.HS512, SEGREDO.getBytes()) //Assinatura/Salt
                                     .compact();

        return token;
    }

    // Metodos para validacao do token
    public String getEmail(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    public Claims getClaims(String token){
        return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean isTokenValido(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();

        Date expDate = claims.getExpiration();
        Date nowDate = new Date(System.currentTimeMillis());

        if(email != null && expDate != null && nowDate.before(expDate)){
            return true;
        }else {
            return false;
        }
    }
}
