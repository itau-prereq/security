package br.com.spring.security.auth;

import br.com.spring.security.models.Usuario;
import br.com.spring.security.services.UsuarioService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroDeAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;
    private UsuarioService usuarioService;

    public FiltroDeAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil, UsuarioService usuarioService) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.usuarioService = usuarioService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            String tokenLimpo = authorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken authToken = getAuthentication(request, tokenLimpo);

            if(authToken != null){
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, String token){
        if (jwtUtil.isTokenValido(token)){
            String email = jwtUtil.getEmail(token);
            UserDetails userDetails = usuarioService.loadUserByUsername(email);
            return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        } else {
            return null;
        }
    }
}
