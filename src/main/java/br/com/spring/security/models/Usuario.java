package br.com.spring.security.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "O nome é obrigatório")
    @Size(min = 3, message = "Nome tem que ter no minimo 3 caracteres")
    private String nome;

    @CPF(message = "CPF invalido")
    @NotNull(message = "CPF é obrigatório")
    private String cpf;

    @Email(message = "Email invalido")
    @NotNull(message = "Email deve vir preenchido")
    @Column(unique = true)
    private String email;

    @NotBlank(message = "Senha deve vir preenchida")
    private String senha;

    public Usuario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
