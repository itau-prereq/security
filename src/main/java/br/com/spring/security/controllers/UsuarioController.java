package br.com.spring.security.controllers;

import br.com.spring.security.models.Usuario;
import br.com.spring.security.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Iterable<Usuario> retornaTodosUsuarios(){
        return usuarioService.retornaTodosUsuarios();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        return usuarioService.salvarUsuario(usuario);
    }
}
