package br.com.spring.security.configurations;

import br.com.spring.security.auth.FiltroDeAutenticacao;
import br.com.spring.security.auth.FiltroDeAutorizacao;
import br.com.spring.security.auth.JWTUtil;
import br.com.spring.security.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UsuarioService usuarioService;

    private static final String[] ENDERECOS_PUBLICOS_POST = {
            "/usuarios",
            "/login"
    };

    private static final String[] ENDERECOS_PUBLICOS_GET = {
            "/usuarios"
    };


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // Primeira coisa a ser verificada(valida endpoints publicos e privados, adiciona os CORS e realiza os filtros
        // Todas as requisições caem aqui

        http.csrf().disable(); // Não valida token
        http.cors();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // padrao é STATEFUL, REST usa STATELESS
        http.authorizeRequests().antMatchers(HttpMethod.POST, ENDERECOS_PUBLICOS_POST).permitAll()
                //.antMatchers(HttpMethod.GET, ENDERECOS_PUBLICOS_GET).permitAll()
                .anyRequest().authenticated();

        // o /login  aplica o filtro da classe que faz [extends UsernamePasswordAuthenticationFilter]
        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));

        // todas as outras rotas não expostas tem que passar pela classe que faz [extends BasicAuthenticationFilter]
        http.addFilter(new FiltroDeAutorizacao(authenticationManager(), jwtUtil, usuarioService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(usuarioService).passwordEncoder(construtorDeEncoder());
    }


    // Define os dominios com o acesso permitido
    @Bean
    CorsConfigurationSource construtorDeCors(){
        UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();

        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());

        return cors;
    }

    @Bean
    BCryptPasswordEncoder construtorDeEncoder(){
        return new BCryptPasswordEncoder();
    }
}
